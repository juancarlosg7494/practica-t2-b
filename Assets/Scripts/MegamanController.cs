using Microsoft.Win32.SafeHandles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MegamanController : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator animator;
    private SpriteRenderer sr;
    public float velocity  = 5;
    public float jump = 30;
    public GameObject rightBullet;
    public GameObject leftBullet;

    public GameObject rightBullet1;
    public GameObject leftBullet1;

    public GameObject rightBullet2;
    public GameObject leftBullet2;
    public float carga = 0f;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetInteger("Estado", 0);

        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.velocity = new Vector2(velocity, rb.velocity.y); 
            sr.flipX = false;
            animator.SetInteger("Estado",1); 
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.velocity = new Vector2(-velocity, rb.velocity.y); 
            sr.flipX = true; 
            animator.SetInteger("Estado",1); 
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            rb.AddForce(Vector2.up * jump, ForceMode2D.Impulse);
            animator.SetInteger("Estado",2); 
        }   
        if (Input.GetKey(KeyCode.X)){
            carga += Time.deltaTime;
            
            if (sr.color == Color.red)
            {
                sr.color= Color.white;
            }else{
                sr.color= Color.red;
            }
        }
        if (Input.GetKeyUp(KeyCode.X))
        {
            animator.SetInteger("Estado",3); 
            sr.color= Color.white;
            if (carga >= 5)
            {
                var bullet = sr.flipX ? leftBullet2 : rightBullet2;
                var position = new Vector2(transform.position.x, transform.position.y);
                var rotation = rightBullet2.transform.rotation;
                Instantiate(bullet, position, rotation);
            }else
            {
                if (carga >= 3)
                {
                    var bullet = sr.flipX ? leftBullet1 : rightBullet1;
                    var position = new Vector2(transform.position.x, transform.position.y);
                    var rotation = rightBullet1.transform.rotation;
                    Instantiate(bullet, position, rotation);
                }else
                {
                    var bullet = sr.flipX ? leftBullet : rightBullet;
                    var position = new Vector2(transform.position.x, transform.position.y);
                    var rotation = rightBullet.transform.rotation;
                    Instantiate(bullet, position, rotation);
                }
            }
            
            
            
            carga = 0;
            
        }
    }
}
