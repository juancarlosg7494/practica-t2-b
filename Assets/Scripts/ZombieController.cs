using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieController : MonoBehaviour
{
    public int vidas = 5;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Bala"))
        {
            vidas -= 1;
            if(vidas <= 0){
                Destroy(this.gameObject);
            }
        }
        if (other.gameObject.CompareTag("Bala1"))
        {
            vidas -= 3;
            if(vidas <= 0){
                Destroy(this.gameObject);
            }
        }
        if (other.gameObject.CompareTag("Bala2"))
        {
            vidas -= 5;
            if(vidas <= 0){
                Destroy(this.gameObject);
            }
        }
    }
}
